class Socket {
    constructor(socket) {
        this.socket = socket;
        socket.on('getAllAccounts', msg => this.getAccountList(msg))
    }

    getAccountList(msg) {
        console.log(msg)
        AccountServices.getAllAccounts((err, rls) => {
            io.to(this.socket.id).emit('getAllAccounts', JSON.stringify(rls))
        })
    }

    receiveFiles(){

    }
}


module.exports = Socket