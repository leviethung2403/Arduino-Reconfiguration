var mongoose = require('mongoose');

var arduinoSchema = new mongoose.Schema({
    controller: {
        type: String,
        required: true,
        index: true
    },
    sketchPath: {
        type: String,
        required: true,
        index: true
    },
    appname:{
        type: String,
        required: true
    },
    version:{
        type: String,
        required: true
    },
    originalname:{
        type: String,
        required: true,
        index: true
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
})

var arduino = mongoose.model('arduino', arduinoSchema)

module.exports = arduino