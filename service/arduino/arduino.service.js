const
    exec = require('child_process').exec,
    mkdirp = require('mkdirp'),
    path = require('path'),
    async = require('async'),
    arduinoModel = require('./arduino.model'),
    lateastVer = require('../latestVer/latestVer.service'),
    mqtt = require('../mqtt/mqtt')

module.exports = class arduino {
    static verify(sketch_path, sketch_folder, callback){
        const
            build_folder = path.join(sketch_folder, 'build'),
            arduino = config.arduino_path

        console.log('sketch_path:',sketch_path)
        console.log('sketch_folder:',sketch_folder)
        console.log('build_folder:',build_folder)

        async.waterfall([
            cb=> mkdirp(build_folder, cb),
            (build_folder, cb)=>{
                const command = `arduino -v --verify ${sketch_path}`
                exec(command, (error, stdout, stderr)=>{
                    if (error) return cb(error)
                    console.log(`stderr: ${stderr}`)
                    console.log(`stdout: ${stdout}`)
                    cb(null, build_folder)
                })
            }
        ], (err, build_folder) => {
            return callback(err, build_folder, sketch_path)
        })
    }
    static rmdir(path, filename, callback){
        exec('rm -r ' + path, (err, stdout, stderr) => {
            return callback(err, filename)
        })
    }
    static store(controller, filename, bodyForm, originalname, callback){
        this.checkAppExist(bodyForm.appname, (err, exist)=>{
            if (err) return callback(err)
            async.parallel([
                cb=>this.addNewSketch(controller, filename, bodyForm.appname, bodyForm.version, originalname, cb),
                cb=>{
                    if (!exist) lateastVer.addNewVersion(controller, bodyForm.appname, bodyForm.version, cb)
                    else lateastVer.updateVersion(controller, bodyForm.appname, bodyForm.version, cb)
                },
                cb=>{
                    let topic = `${controller}/${bodyForm.appname}`
                    console.log('publish package.' + topic)
                    mqtt.publish(topic, bodyForm.version, cb)
                }
            ], callback)
        })

    }
    static addNewSketch(controller, filename, appname, version, originalname, callback){
        const query = {
            controller: controller,
            sketchPath: filename,
            appname: appname,
            version: version,
            originalname: originalname
        }
        arduinoModel.create(query, callback)
    }
    static checkAppExist(appname, callback){
        arduinoModel.findOne({appname: appname}, (err, app)=>{
            if (err) return callback(err)
            if (app){
                return callback(null, true, app)
            } else {
                return callback(null, false)
            }
        })
    }
    static getSketchPath(controller, appname, version, callback){
        const condition = {
            controller: controller,
            appname: appname,
            version: version
        }
        arduinoModel.findOne(condition, callback)
    }
}