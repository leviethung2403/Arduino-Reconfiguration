const
    mqtt = require('mqtt'),
    client = mqtt.connect('mqtt://127.0.0.1')

module.exports = class mqttClass {
    constructor(){
        client.on('connect', () => {
            console.log('connected to MQTT')
        })
    }
    static publish(topic, message, callback) {
        client.publish(topic, message, callback)
    }
}