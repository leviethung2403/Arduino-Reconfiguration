
const
    User = require('../account/accounts.service'),
    Passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    bcryptjs = require('bcryptjs'),
    accountModel = require('../account/accounts.model')

module.exports = (app)=> {
    app.route('/login')
        .get((req, res) => {
            res.render('login', {mess: ''})
        })
        .post(Passport.authenticate('local', {
            failureRedirect: '/login/error',
            successRedirect: '/',
            failureFlash: true
        }), (req, res) => {
            res.redirect('/')
        })
    app.route('/login/:mess')
        .get((req, res) => {
            if (req.params.mess == 'error')
                res.render('login', {mess: 'Lỗi đăng nhập, sai tài khoản hoặc mật khẩu!'})
            else res.render('login', {mess: ''})
        })
        .post(Passport.authenticate('local', {
            failureRedirect: '/login/error',
            successRedirect: '/',
            failureFlash: true
        }), (req, res) => {
            res.redirect('/')
        })

    app.get('/logout', (req, res) => {
        req.logout()
        req.flash('success_msg', 'You are logged out')
        res.redirect('/')
    })
    Passport.use(new LocalStrategy(
        (username, password, done) => {
            accountModel.findOne({username: username}, (err_, user) => {
                if (err_) return done(err_)
                if (user) {
                    bcryptjs.compare(password, user.password, (err, isMatch) => {
                        if (err) return done(err)
                        console.log('Authenticated:', isMatch)
                        if (isMatch) return done(null, user)
                        return done(null, false)
                    })
                } else return done(null, false)
            })
        }
    ))
    Passport.serializeUser((user, next) => {
        next(null, user.id)
    })

    Passport.deserializeUser((id, next) => {
        User.getUserById(id, next)
    })
}