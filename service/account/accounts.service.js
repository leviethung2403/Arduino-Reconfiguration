
const account = require('./accounts.model')

module.exports = {
    getUserById: (id, callback) => {
        account.findById(id, '-password', (callback))
    },

    getUserByUsername: (username, callback) => {
        account.findOne(
            {username: username},
            callback
        )
    }
}