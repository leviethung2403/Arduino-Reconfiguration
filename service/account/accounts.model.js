var mongoose = require('mongoose');

var accountsSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        index: true
    },
    password: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
})

var account = mongoose.model('account', accountsSchema)

module.exports = account