var mongoose = require('mongoose');

var latestVerSchema = new mongoose.Schema({
    controller:{
        type: String,
        required: true,
        index: true
    },
    appname:{
        type: String,
        required: true,
        index: true
    },
    version:{
        type: String,
        required: true
    },
    updateAt:{
        type: Date,
        default: new Date()
    },
    createdAt: {
        type: Date
    }
})

var lateastVersion = mongoose.model('lateastVersion', latestVerSchema)

module.exports = lateastVersion