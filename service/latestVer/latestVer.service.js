const
    latestVersion = require('./latestVer.model')

module.exports = class arduino {
    static updateVersion(controller, appname, ver, callback){
        console.log('updateVersion')
        const
            condition = { appname: appname, controller: controller},
            update = { version: ver}
        latestVersion.findOneAndUpdate(condition, update, callback)
    }
    static addNewVersion(controller, appname, ver, callback){
        console.log('addNewVersion')
        const createdAt = new Date()
        const param = {
            controller: controller,
            appname: appname,
            version: ver,
            createdAt: createdAt
        }
        latestVersion.create(param, callback)
    }
    static showAll(controller, callback){
        latestVersion.find({}, callback)
    }
    static getLastVersion(controller, appname, callback){
        const condition = {
            controller: controller,
            appname: appname
        }
        latestVersion.findOne(condition, (err, res)=>{
            if (!res) return callback('Author or version not matching.')
            return callback(err, res)
        })
    }
}