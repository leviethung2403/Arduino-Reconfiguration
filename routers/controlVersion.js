
const
    express = require('express'),
    router = express.Router(),
    latestVersion = require('../service/latestVer/latestVer.service')

router.get('/', (req, res)=>{
    if (req.isAuthenticated()) {
        latestVersion.showAll(req.user.username, (err, allApp) => {
            res.render('table_view', {
                title: 'Quản lý phiên bản phần mềm',
                logged: req.isAuthenticated(),
                page: 'phien-ban',
                result: allApp,
                boxTitle: 'Phiên bản mới nhất',
                titleCol: ['STT', 'TÊN ỨNG DỤNG', 'SỐ PHIÊN BẢN'],
                profile_pic: '5a7b9e9f5b81bf2e28ce5e23_logo.jpg',
                user: req.user
            })
        })
    } else {
        res.redirect('/login')
    }
})
// router.post('/', (req, res)=>{
//     ...
// })


module.exports = router;
