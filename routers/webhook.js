

const 
	express = require('express'),
	router = express.Router(),
	config = require('config'),
	messageProcess = require('./receivedMessage'),
  api = require('./../api/APIs.js')


const APP_SECRET = config.get('appSecret');
const VALIDATION_TOKEN = config.get('validationToken');
const PAGE_ACCESS_TOKEN = config.get('pageAccessToken');
const SERVER_URL = config.get('serverURL');

if (!(APP_SECRET && VALIDATION_TOKEN && PAGE_ACCESS_TOKEN && SERVER_URL)) {
  console.error("Missing config values");
  process.exit(1);
}

router.get('/', function(req, res) {
  if (req.query['hub.mode'] === 'subscribe' &&
      req.query['hub.verify_token'] === VALIDATION_TOKEN) { // So sánh VALIDATION_TOKEN (my_token) mà bạn đã điền vào trong ô của facebook app với giá trị mà bạn đặt trong config/default.json. Nếu 2 cái này khớp thì verify thành công.
    console.log("Validating webhook");
    res.status(200).send(req.query['hub.challenge']);
  } else {
    console.error("Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);          
  }  
});

router.post('/', function (req, res) {
  var data = req.body;

  if (data.object == 'page') {
    data.entry.forEach(function(pageEntry) {
      api.getFullInfo (pageEntry.messaging, function (user) {
        user.forEach(function(messagingEvent) {
          if (messagingEvent.message) {
            messageProcess.receivedMessage(messagingEvent);
          } else if (messagingEvent.delivery) {
            messageProcess.receivedDeliveryConfirmation(messagingEvent);
          } else if (messagingEvent.postback) {
            messageProcess.receivedPostback(messagingEvent);
          } else if (messagingEvent.read) {
            messageProcess.receivedMessageRead(messagingEvent);
          } else if (messagingEvent.account_linking) {
            messageProcess.receivedAccountLink(messagingEvent);
          } else {
            console.log("Webhook received unknown messagingEvent: ", messagingEvent);
          }
        });
      });
    });
    res.sendStatus(200);
  }
});

/*
 * This path is used for account linking. The account linking call-to-action
 * (sendAccountLinking) is pointed to this URL. 
 * 
 */
 
// Phục vụ cho account linking (khi người dùng cần đăng nhập trước khi chat với bot) -> phần này chưa cần quan tâm đến.
router.get('/authorize', function(req, res) {
  var accountLinkingToken = req.query['account_linking_token'];
  var redirectURI = req.query['redirect_uri'];

  // Authorization Code should be generated per user by the developer. This will 
  // be passed to the Account Linking callback.
  var authCode = "1234567890";

  // Redirect users to this URI on successful login
  var redirectURISuccess = redirectURI + "&authorization_code=" + authCode;

  res.render('authorize', {
    accountLinkingToken: accountLinkingToken,
    redirectURI: redirectURI,
    redirectURISuccess: redirectURISuccess
  });
});

module.exports = router;