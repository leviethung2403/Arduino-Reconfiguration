
const
    express = require('express'),
    router = express.Router(),
    arduino = require('../service/arduino/arduino.service'),
    latestVersion = require('../service/latestVer/latestVer.service'),
    async = require('async')

router.post('/get-sketch-path', (req, res)=>{
    if (req.body && req.body.appname && req.body.controller){
        async.waterfall([
            cb=> latestVersion.getLastVersion(req.body.controller, req.body.appname, cb),
            (result, cb)=> arduino.getSketchPath(result.controller, result.appname, result.version, cb)
        ], (err, result)=>{
            if (err) return res.status(404).json({Error: err})
            res.status(200).json({message: result.sketchPath})
        })
    } else {
        res.status(404).json({Error: 'Params are required.'})
    }
})


module.exports = router;
