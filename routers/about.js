

var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');
var sendEmail = require('./../api/sendEmail');
var mongoose = require('mongoose');

// router.get('/abc/:id/:code', (req, res) => {
// 	var id = req.params['id'];
// 	var code = req.params['code'];
// });

router.get('/', (req, res) => {
	if (req.isAuthenticated())
		res.render('about', {profile_pic: req.user.profile_pic, page: 'contact', logged : true, name: req.user.uid, donvi: req.user.username, role: req.user.role});
	else
		res.render('about', {page: 'contact', logged : false});
});

router.post('/', (req, res) => {
	var text = "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'> </head><body>" + req.body.txtName + " <" + req.body.email + ">" + "<br>" + req.body.message + "</body></html>";
	var temp = new Array();
	//Tìm những tài khoản có role là 1 và 2
	temp.push({role: 1});
	temp.push({role: 2});
	var query = {$or : temp}
	mongoose.model('account_list').find(query).exec(function(err, result) {
		for (var i = 0; i < result.length; i++) {
			console.log(result[i].email);
			console.log(text);
			sendEmail.Send(result[i].email, 'Góp ý cho wabisabi', text)
		}
		res.redirect('/lien-he');
	})
});
module.exports = router;
