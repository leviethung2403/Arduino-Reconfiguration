var api = require('./../api/APIs.js'),
	express = require('express'),
	router = express.Router(),
	mongoose = require('mongoose'),
	sendEmail = require('./../api/sendEmail')

function addNumberZero (number){
	switch (Number(number)) {
		case 1: number = '01';
		break;
		case 2: number = '02';
		break;
		case 3: number = '03';
		break;
		case 4: number = '04';
		break;
		case 5: number = '05';
		break;
		case 6: number = '06';
		break;
		case 7: number = '07';
		break;
		case 8: number = '08';
		break;
		case 9: number = '09';
		break;
		default:number = '00';
	}
	return number;
}

function sendNotifyToUser(user_id, noti) {
	mongoose.model('connect').find({user_id: user_id}, function(err, userList) {
		if (err) throw err;

		var year = noti.date.getFullYear(),
			month = noti.date.getMonth()+1,
			day = noti.date.getDate(),
			hour = noti.date.getHours(),
			minute = noti.date.getMinutes()

		if (minute < 10)
			minute = addNumberZero(minute);

		if (userList){
			userList.map(item => {
				let recipientId = item.fb_id
				let messageData1 = {
				  text: "🎉 " + noti.schoolName + " vừa gửi đến bạn một thông báo sự kiện sắp diễn ra: " + noti.activity_name
				}
                let messageData2 = {
                    attachment: {
                        type: "template",
                        payload: {
                            template_type: "button",
                            text: `🍀 Sẽ diễn ra vào lúc ${hour}g${minute} ngày ${day} tháng ${month} năm ${year} \n🍀 Tại ${noti.position}`,
                            buttons:[{
                                type: "web_url",
                                url: noti.linkDetail,
                                title: "Thông tin chi tiết"
                            }, {
                                type: "postback",
                                title: "Đăng ký tham gia",
                                payload:"USER_PICK_REGISTER " + noti._id
                            }]
                        }
                    }
                }
				let messages = []
				messages.push(messageData1)
				messages.push(messageData2)
				api.sendMultiMessageWithChatbot(recipientId, messages)
			})
		}
	})
}

function sendMessageToUser(user_id, noti) {
	mongoose.model('connect').find({user_id: user_id}, function(err, userList) {
		if (err) throw err;

		if (userList){
			userList.map(item=> {
				var recipientId = item.fb_id;

				var messageData1 = {
						text: "🎉 " + noti.schoolName + " vừa gửi đến bạn một tin nhắn " + noti.titleMessage + ": "
				}
				var messageData2 = {
						text: noti.content
				}
				let messages = []
				messages.push(messageData1)
				messages.push(messageData2)
				api.sendMultiMessageWithChatbot(recipientId, messages)
			})
		}
	});
}

router.get('/announcement/:schoolid/:uid/:verify_code/:result', (req, res) => {
	var school = req.params.schoolid,
		uid = req.params.uid,
		verify_code = req.params.verify_code,
		result = req.params.result,
		query = {_id: uid, verify_code: verify_code, checkSent: false},
		// query = {_id: uid, verify_code: verify_code}, // xóa dòng này sau khi thử.
		update = {checkSent: true},
		option = {new : false}

	if (result === 'accept') {
		mongoose.model('announcement').findOneAndUpdate(query, update, option, function (err, result){
			if (err) throw err;
			if (result) {
				sendNotifyToUser(school, result);
				res.redirect('/');
			} else {
				console.log("Lỗi khi query, không tìm thấy hoạt động cần xác nhận hoặc hoạt động đó đã được xác nhận rồi. Gửi phản hồi cho người dùng biết tại đây.");
				res.redirect('/');
			}
		});
	} else if(result === 'remove'){
		mongoose.model('announcement').findOneAndUpdate(query, update, option, function (err, result){
			if (err) throw err;
			if (result) {
				var query = {uid: req.user.uid}
				mongoose.model('account_list').findOne(query).exec(function(err, result) {
					sendEmail.Send(result.email, 'Hoạt động không được xác nhận', 'Xin lỗi hoạt động của bạn không được xác nhận');
				});
				res.redirect('/');
			} else {
				console.log("Lỗi khi query, không tìm thấy hoạt động cần xác nhận hoặc hoạt động đó đã được xác nhận rồi. Gửi phản hồi cho người dùng biết tại đây.");
				res.redirect('/');
			}
		});
	}
});

router.get('/messageToFollower/:schoolid/:messageId/:verify_code/:result', (req, res) => {
	var school = req.params.schoolid,
		messageId = req.params.messageId,
		verify_code = req.params.verify_code,
		result = req.params.result,
		query = {_id: messageId, verify_code: verify_code, checkSent: false},
		// query = {_id: messageId, verify_code: verify_code}, // xóa dòng này sau khi thử.
		update = {checkSent: true},
		option = {new : false}

	if (result === 'accept') {
		mongoose.model('messageToFollower').findOneAndUpdate(query, update, option, function (err, result){
			if (err) throw err;
			if (result) {
				sendMessageToUser(school, result);
				res.redirect('/');
			} else {
				console.log("Lỗi khi query, không tìm thấy hoạt động cần xác nhận hoặc hoạt động đó đã được xác nhận rồi. Gửi phản hồi cho người dùng biết tại đây.");
				res.redirect('/');
			}
		});
	} else if(result === 'remove'){
		mongoose.model('messageToFollower').findOneAndUpdate(query, update, option, function (err, result){
			if (err) throw err;
			if (result) {
				var query = {uid: req.user.uid}
				mongoose.model('account_list').findOne(query).exec(function(err, result) {
					sendEmail.Send(result.email, 'Hoạt động không được xác nhận', 'Xin lỗi hoạt động của bạn không được xác nhận');
				});
				res.redirect('/');
			} else {
				console.log("Lỗi khi query, không tìm thấy hoạt động cần xác nhận hoặc hoạt động đó đã được xác nhận rồi. Gửi phản hồi cho người dùng biết tại đây.");
				res.redirect('/');
			}
		});
	}
});

module.exports = router;
