var express = require('express'),
	router = express.Router(),
	mongoose = require('mongoose'),
	today = new Date();


router.get('/404.html', (req, res) => {
	if (req.isAuthenticated())
		res.render('404', {profile_pic: req.user.profile_pic, page: '404', logged : true, name: req.user.uid, donvi: req.user.username, role: req.user.role});
	else
		res.render('404', {profile_pic: req.user.profile_pic, page: '404', logged : false});
});

router.get('/500.html', (req, res) => {
	if (req.isAuthenticated())
		res.render('500', {profile_pic: req.user.profile_pic, page: '500', logged : true, name: req.user.uid, donvi: req.user.username, role: req.user.role});
	else
		res.render('500', {profile_pic: req.user.profile_pic, page: '500', logged : false});
});


module.exports = router;
