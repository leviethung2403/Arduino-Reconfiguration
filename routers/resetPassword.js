var mongoose = require('mongoose');
var express = require('express');
var router = express.Router();
var sendEmail = require('./../api/sendEmail');
var randomstring = require("randomstring");
var config = require('config');
var changePassword = require('./../api/changePassword');
var	SERVER_URL = config.get('serverURL');


router.get('/', (req, res) => {
  res.render('resetPassword', {mess: ''})
});

router.post('/', (req, res) => {
  var query = {uid: req.body.uid}
  mongoose.model('account_list').findOne(query).exec(function (err, result) {
    if (err) throw err;
    if (!result)
      res.render('resetPassword', {mess: 'Tài khoản không tồn tại, xin vui lòng nhập lại'});
    else {
      var query = {uid: req.body.uid}
      mongoose.model('resetPassword').findOne(query).exec(function (err, result1) {
        if (err) throw err;
        if (result1) {
          mongoose.model('resetPassword').remove(query).exec(function (err, result2) {
            if (err) throw err;
            res.redirect('/');
          })
        }
        else {
          var randoms = randomstring.generate(50);
          var textLink = "https://" + SERVER_URL + '/resetPassword' + '/' + req.body.uid +'/' + randoms;
          insert = {
            email: result.email,
            randomCode: randoms,
            uid: result.uid
          };
          mongoose.model('resetPassword').create(insert, function (err, result1) {
            if (err) throw err;
            //Giai đoạn chưa có HTML Template
            sendEmail.Send(result.email, "Xác nhận lại mật khẩu", textLink);
            res.render('resetPassword', {mess: 'Vui lòng kiểm tra lại email'});
          })
        }
      })
    }
  })
})


router.get('/:uid/:randomCode', (req, res) => {
	var query = {
		uid: req.params.uid,
		randomCode: req.params.randomCode,
		checkSent: false
	}
	var update = {
		checkSent: true
	};
	var option = {
		new: false
	}
	mongoose.model('resetPassword').findOneAndUpdate(query, update, option, function (err, result) {
		if (err) throw err;
		if (result)
			res.render('newPassword', {mess: '', uid: result.uid, id: result._id, randoms: result.randomCode});
		else res.render('404');
	})
});

router.post('/:uid/:id/:randoms', (req, res) => {
	var query = {
		uid: req.params.uid,
		_id: req.params.id,
    randomCode: req.params.randoms
	};
	mongoose.model('resetPassword').findOne(query).exec(function (err, result) {
		if (err) throw err;
		if (result) {
			if (req.body.password != "" && req.body.reTypePass != "" && req.body.password == req.body.reTypePass) {
				changePassword.CheckWithoutCompare(req.params.uid, req.body.password, req.body.reTypePass);
				mongoose.model('resetPassword').remove(query).exec(function (err, result1) {
					if (err) throw err;
				});
				res.redirect('/');
			}
			else {
				res.render('newPassword', {mess: 'Mật khẩu không khớp, xin vui lòng nhập lại!', uid: result.uid, id: result._id, randoms: result.randomCode})
			}
		}
	})
})


module.exports = router;
