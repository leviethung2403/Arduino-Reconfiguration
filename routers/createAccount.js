

var express = require('express'),
	router = express.Router(),
	nodemailer = require('nodemailer'),
	funcSaveSchool = require('./../api/createNewSchool'),
	mongoose = require('mongoose'),
	bcrypt = require('bcryptjs'),
	ejs = require('ejs'),
	randomstring = require("randomstring");

router.get('/truong', (req, res) => { //only admin, dev
	if (req.isAuthenticated()) {
		if (req.user.role < 3) {
			res.render('addSchool', {profile_pic: req.user.profile_pic, user: req.user})
		} else if (req.user.role < 4){
			res.redirect('/them-tai-khoan/khoa');
		} else {
			res.redirect('/error');
		}
	} else {
		res.render('404', {page: '404'});
	};
});

router.get('/khoa', (req, res) => { //School can add account
	if (req.isAuthenticated()) {
		if (req.user.role === 3) {
			res.render('addSchool', {profile_pic: req.user.profile_pic, user: req.user})
		} else if (req.user.role < 3){
			res.redirect('/them-tai-khoan/truong');
		}
	} else {
		res.redirect('/login');
	};
});

router.post('/truong', (req, res) => { // gửi thông tin tạo tài khoản cấp trường, vừa tạo cho cấp khoa.
	var newPass = randomstring.generate(15);
	let userID = req.body.schoolID;
	let school_id = userID,
		schoolname = req.body.schoolName,
		email = req.body.email,
		password = newPass,
		role = req.body.role;

	if (req.isAuthenticated()) {
		if (req.body.role == 4){
			userID = req.user.uid + "." + userID;
			school_id = userID;
		}
		mongoose.model('account_list').find({uid: school_id}, function(err, found){
			if (found[0] == null){
				bcrypt.hash(password, 10, function(err, hash) { //use password default
					funcSaveSchool.Save(school_id, schoolname, email, role, password, hash, req.user.uid);
				})
				res.render('addSchool', {profile_pic: req.user.profile_pic, user: req.user, msg_suc: "Đã tạo tài khoản thành công."})
			} else {
				res.render('addSchool', {profile_pic: req.user.profile_pic, user: req.user, msg_err: "Tài khoản đã được đăng ký, vui lòng sử dụng tên đăng nhập khác hoặc sử dụng chức năng quên mật khẩu."})
			};
		});
	} else {
		res.redirect('/login');
	}
});


module.exports = router;
