var multer = require('multer');
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var imageLink = '';

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/images/uploads/')
    },
    filename: function (req, file, callback) {
        let temp = file.originalname.split('.');
        imageLink = req.user._id + "_logo." + temp[1];
        callback(null, imageLink);
    }
});

var upload = multer({ storage: storage });

router.get("/", function (req, res) {
    res.redirect('/thong-tin');
});

router.post("/", upload.any(), function (req, res) {
    if (req.isAuthenticated()) {
        let query = {
            uid: req.user.uid,
            username: req.user.username
        }
        let update = {
            profile_pic: imageLink
        }
        let option = {
            new: false
        }
        mongoose.model('account_list').findOneAndUpdate(query, update, option, function (err, result) {
            if (err) throw err;
            res.redirect('/thong-tin');
        })
    } else {
        res.redirect('/login');
    }
});

module.exports = router;
