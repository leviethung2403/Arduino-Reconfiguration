var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var changePassword = require('./../api/changePassword');
var ejs = require('ejs');
var config = require('config');
var	SERVER_URL = config.get('serverURL');
var randomstring = require("randomstring");
var getTemplate = require('./../api/getEmailTemplate');
var sendEmail = require('./../api/sendEmail');


function addNumberZero (number){
    return ('0'+number).slice(-2);
}

var tabvalues = 'maininfo';
router.get('/', (req, res) => {
	if (req.isAuthenticated()) {
		if (req.user.role <= 2) {
			//Nếu quyền tài khoản là 1, 2 thì set thêm tính năng nhận email, quản lý khoa và trường
			rendermain = {
				logged: true,
				page: 'manager',
				role: req.user.role,
				donvi: req.user.username,
				tab: tabvalues,
				email: req.user.email,
				name: req.user.uid,
				thuoccap: req.user.schoolOwner,
				address: req.user.address,
				profile_pic: req.user.profile_pic,
				theodoi: 0,
			};
			//Tìm tài khoản các trường thuộc cấp 
			var query = {schoolOwner: req.user.uid}; 
			mongoose.model('account_list').find(query).exec(function(err, result) { 
				if (err) throw err; 
				render = { 
					belongTo: result, 
				}; 
				var sort = {date: -1}; 
				//List tất cả các hoạt động 
				mongoose.model('announcement').find().sort(sort).exec(function(err, result2) { 
					if (err) throw err; 
					render1 = { 
						activities: result2, 
					} 
					res.render('Dashboard/main',  Object.assign(rendermain, render, render1)); 
				}) 
			}) 
		}
		else if (req.user.role == 3) {
			//Nếu quyền tài khoản là  3 thì set thêm tính năng nhận email, quản lý khoa
			rendermain = {
				logged: true,
				page: 'manager',
				tab: tabvalues,
				donvi: req.user.username,
				role: req.user.role,
				email: req.user.email,
				name: req.user.uid,
				thuoccap: req.user.schoolOwner,
				address: req.user.address,
				profile_pic: req.user.profile_pic,
				theodoi: 0,
			};
			mongoose.model('connect').find({}, function (err, connectDoc){
				if (err) throw err;
				if (connectDoc){
					rendermain.theodoi = connectDoc.length;
					//Tìm tài khoản các khoa thuộc cấp
					var query = {schoolOwner: req.user.uid};
					mongoose.model('account_list').find(query).exec(function(err, result) {
						if (err) throw err;
						render = {
							belongTo: result,
						};
						var temp = new Array();
						temp.push({school_id: req.user.uid})
						for (var i = 0; i < result.length; i++) {
							temp.push({school_id: result[i].uid});
						};
						var query1 = {$or:temp};
						var sort = {date: -1};
						mongoose.model('announcement').find(query1).sort(sort).exec(function(err, result2) {
							if (err) throw err;
							render2 = {
								activities: result2,
							}
							//console.log(Object.assign(rendermain, render, render1, render2));
							res.render('Dashboard/main',  Object.assign(rendermain, render, render2));
						})
					})
				}
			})
			
		}
		else if (req.user.role == 4) {
			// Nếu quyền = 4 thì chỉ được tháy thông tin quản lý tài khoản
			render = {
				logged: true,
				page: 'manager',
				role: req.user.role,
				email: req.user.email,
				donvi: req.user.username,
				tab: tabvalues,
				name: req.user.uid,
				thuoccap: req.user.schoolOwner,
				address: req.user.email,
				profile_pic: req.user.profile_pic,
				theodoi: 0,
			};
			mongoose.model('connect').find({}, function (err, connectDoc){
				if (err) throw err;
				if (connectDoc){
					rendermain.theodoi = connectDoc.length;

					let query1 = {school_id: req.user.uid};
					let sort = {date: -1};
					mongoose.model('announcement').find(query1).sort(sort).exec(function(err, result2) {
						if (err) throw err;
						render2 = {
							activities: result2,
						}
						//console.log(Object.assign(render, render2));
						res.render('Dashboard/main',  Object.assign(render, render2));
					})
				}
			})
		}
	} else {
		res.redirect('/login');
	}

});

router.post('/', (req, res) => {
	if(req.isAuthenticated()) {
		if (req.body.loailenh == 'changeAccountPassword') {
			if (req.body.new_password == "") {
				var update = {address: req.body.address};
				var query = {uid: req.user.uid};
				var option = {new: false};
				mongoose.model('account_list').findOneAndUpdate(query, update, option, function (err, ketqua) {
					if (err) throw err;
					if (ketqua) console.log("Thay đổi thành công");
					else console.log("Thay đổi thất bại");
				})
			}
			else if (req.body.new_password != "" && req.body.reTypePass != "" && req.body.current_pass != "")
				changePassword.Check(req.user.uid, req.body.current_pass, req.body.new_password, req.body.reType_password, req.user.password);
			tabvalues = 'maininfo';
			res.redirect('/thong-tin');
		}
		else if (req.body.loailenh == 'addEmailReceive') {
			var insert = {schoolOwner: req.user.uid, name: req.body.newName, email: req.body.newEmail}
			mongoose.model('accountNotify').create(insert, function(err, result) {
				if (err) throw err;
				console.log("Đã thêm thành công");
				console.log(result);
				tabvalues = 'accounts-received-email';
				res.redirect('/thong-tin');
			})
		}
		else if (req.body.loailenh == 'changeFacultyPassword') {
			var query = {schoolOwner: req.user.uid};
			mongoose.model('account_list').find(query).exec(function(err, result) {
				for (var i =0; i< result.length; i++) {
					if (req.body.id == result[i]._id) {
						changePassword.CheckWithoutCompare(result[i].uid, req.body.password, req.body.reTypePass);
						tabvalues = 'accounts-faculty';
						res.redirect('/thong-tin');
					}
				}
			})
		}
		else if (req.body.loailenh == 'changeFacultyInfo') {
			var query = {schoolOwner: req.user.uid};
			mongoose.model('account_list').find(query).exec(function(err, result) {
				for (var i =0; i< result.length; i++) {
					if (req.body.id == result[i]._id) {
						var query = {uid: result[i].uid};
						var update = {username: req.body.username, email: req.body.email};
						var option = {new: false};
						mongoose.model('account_list').findOneAndUpdate(query, update, option, function (err, ketqua) {
							if (err) throw err;
							tabvalues = 'accounts-faculty';
							res.redirect('/thong-tin');
						})
					}
				}
			})
		}
		else if (req.body.loailenh == 'deleteFacultyAccount') {
			var query = {schoolOwner: req.user.uid};
			mongoose.model('account_list').find(query).exec(function(err, result) {
				for (var i =0; i< result.length; i++) {
					if (req.body.id == result[i]._id) {
						var query = {uid: result[i].uid};
						mongoose.model('account_list').remove(query, function (err, ketqua) {
							if (err) throw err;
							tabvalues = 'accounts-faculty';
							res.redirect('/thong-tin');
						})
					}
				}
			})
		}
		else if (req.body.loailenh == 'changeActivityInfo') {
			// Nếu quyền là 1, 2 thì liệt kết tất cả các hoạt động
			if (req.user.role <= 2) {
				var query = {schoolOwner: req.user.uid};
				mongoose.model('account_list').find(query).exec(function(err, result1) {
					var sort = {date: -1};
					mongoose.model('announcement').find().sort(sort).exec(function(err, result) {
						if (err) throw err;
						for (var i = 0; i < result.length; i++) {
							if (req.body.id == result[i]._id) {
								var time = req.body.time_of_event.split(':')
									date_ = req.body.day_of_event.split('/')
								var temp = time[1].split(' ')
								var hour = Number(time[0]);
								var minute = Number(temp[0]);
								if (temp[1] === 'PM') {
									hour += 12
								}
								if (hour < 10)
									hour = addNumberZero(hour);
								if (minute < 10)
									minute = addNumberZero(minute);
								var dateTime = date_[2] + "-" + date_[1] + "-" + date_[0]+"T"+hour+":"+minute+"+07:00";
								dateTime = new Date(dateTime);
								var query = {activity_name: result[i].activity_name, date: result[i].date};
								var update = {date: dateTime, position: req.body.alterPosition,introduction: req.body.alterInfor, checkSent:true, linkDetail: req.body.alterLinkDetail};
								var option = {new: false};
								mongoose.model('announcement').findOneAndUpdate(query, update, option).exec(function(err, ketqua) {
									if (err) throw err;
									tabvalues = 'manager-activities';
									res.redirect('/thong-tin');
								})
							}
						}
					})
				})
			}
			// Nếu quyền là 3 thì liệt kê cả những hoạt động cấp khoa và trường
			else if (req.user.role == 3) {
				var query = {schoolOwner: req.user.uid};
				mongoose.model('account_list').find(query).exec(function(err, result1) {
					var temp = new Array();
					temp.push({school_id: req.user.uid})
					for (var i = 0; i < result.length; i++) {
						temp.push({school_id: result[i].uid});
					};
					var query = { $or: temp};
					var sort = {date: -1};
					mongoose.model('announcement').find(query).sort(sort).exec(function(err, result) {
						if (err) throw err;
						for (var i = 0; i < result.length; i++) {
							if (req.body.id == result[i]._id) {
								var time = req.body.time_of_event.split(':'),
									date_ = req.body.day_of_event.split('/');
								var hour = Number(time[0]);
								var minute = Number(temp[0]);
								if (temp[1] === 'PM') {
									hour += 12
								}
								if (hour < 10)
									hour = addNumberZero(hour);
								if (minute < 10)
									minute = addNumberZero(minute);
								var dateTime = date_[2] + "-" + date_[1] + "-" + date_[0]+"T"+hour+":"+minute+"+07:00";
								dateTime = new Date(dateTime);
								var query = {activity_name: result[i].activity_name, date: result[i].date};
								var update = {date: dateTime, position: req.body.alterPosition,introduction: req.body.alterInfor, linkDetail: req.body.alterLinkDetail, checkSent: true};
								var option = {new: false};
								mongoose.model('announcement').findOneAndUpdate(query, update, option).exec(function(err, ketqua) {
									if (err) throw err;
									tabvalues = 'manager-activities';
									res.redirect('/thong-tin');
								})
							}
						}
					})
				})
			}
			else if (req.user.role == 4) {
				var query1 = {school_id: req.user.uid};
				var sort = {date: -1};
				mongoose.model('announcement').find(query1).sort(sort).exec(function(err, result) {
					for (var i = 0; i < result.length; i++) {
						if (req.body.id == result[i]._id) {
							var time = req.body.time_of_event.split(':')
								date_ = req.body.day_of_event.split('/')
							var temp = time[1].split(' ')
							var hour = Number(time[0]);
							var minute = Number(temp[0]);
							if (temp[1] === 'PM') {
								hour += 12
							}
							if (hour < 10)
								hour = addNumberZero(hour);
							if (minute < 10)
								minute = addNumberZero(minute);
							var dateTime = date_[2] + "-" + date_[1] + "-" + date_[0]+"T"+hour+":"+minute+"+07:00";
							dateTime = new Date(dateTime);
							var query = {_id: result[i]._id};
							var oneTimeCode = randomstring.generate(50);
							var update = {date: dateTime, position: req.body.alterPosition,introduction: req.body.alterInfor, linkDetail: req.body.alterLinkDetail, checkSent: false, verify_code: oneTimeCode};
							var option = {new: false};
							mongoose.model('announcement').findOneAndUpdate(query, update, option).exec(function(err, ketqua) {
								if (err) throw err;

								//Nếu là tài khoản khoa sau khi thay đổi nội dung hoạt động thì sẽ phải xác nhận lại
								var textLink = "https://" + SERVER_URL + '/verify/announcement/' + req.user.id +'/' + ketqua._id +'/' + oneTimeCode +'/accept';
								var textLinkRemove = "https://" + SERVER_URL + '/verify/announcement/' + req.user.id +'/' + ketqua._id +'/' + oneTimeCode + '/remove';

								var loaihoatdong = "";
				//
								// if (ketqua[i].tag.sv5t == true) loaihoatdong += " Sinh viên 5 tốt,";
								// if (ketqua[i].tag.skill == true) loaihoatdong += " Kỹ năng sinh viên,";
								// if (ketqua[i].tag.academy == true) loaihoatdong += " Cuộc thi học thuật,";
								// if (ketqua[i].tag.volunteer == true) loaihoatdong += " Tình nguyện vì cộng đồng,";
								// if (ketqua[i].tag.integration == true) loaihoatdong += " Hội nhập quốc tế,";
								// if (ketqua[i].tag.skill == true) loaihoatdong += " Tư vấn, hỗ trợ sinh viên,";
								// if (ketqua[i].tag.leaders == true) loaihoatdong += " Công tác cán bộ";

								var tempa = getTemplate.getTemplateVerifyActivity();
								ejs.renderFile(tempa, {
									tenhoatdong: ketqua.activity_name,
									thoigian: dateTime,
									diadiem: ketqua.position,
									mota: ketqua.introduction,
									motalink: ketqua.linkDetail,
									donvi: ketqua.schoolName,
									loaihoatdong: ketqua.tag,
									xacnhan: textLink.toString(),
									huy: textLinkRemove.toString()
								}, function(err, html){
									console.log("Bước 2");
									var subject = "Xác nhận thay đổi hoạt động";
									//Gửi Email thông qua API sendMail
									var query = {uid: req.user.uid}
									mongoose.model('accountNotify').find(query).exec(function(err, result) {
										for (var i=0; i< result.length; i++) {
											sendEmail.Send(result[i].email, subject, html);
										}
									});
								});
								tabvalues = 'manager-activities';
								res.redirect('/thong-tin');
							})
						}
					}
				})
			}

		}
		else if (req.body.loailenh == 'reconfirm') {
			// Nếu quyền là 1, 2 thì liệt kết tất cả các hoạt động

			if (req.user.role <= 2) {
				var query = {schoolOwner: req.user.uid};
				mongoose.model('account_list').find(query).exec(function(err, result1) {
					var sort = {date: -1};
					mongoose.model('announcement').find().sort(sort).exec(function(err, result) {
						if (err) throw err;
						for (var i = 0; i < result.length; i++) {
							if (req.body.id == result[i]._id) {
								var textLink = "https://" + SERVER_URL + '/verify/announcement/' + req.user.id +'/' + result[i]._id +'/' + result[i].oneTimeCode +'/accept';
								var textLinkRemove = "https://" + SERVER_URL + '/verify/announcement/' + req.user.id +'/' + result[i]._id +'/' + result[i].oneTimeCode + '/remove';
								var tempa = getTemplate.getTemplateVerifyActivity();
								ejs.renderFile(tempa, {
									tenhoatdong: result[i].activity_name,
									thoigian: dateTime,
									diadiem: result[i].position,
									mota: result[i].introduction,
									motalink: result[i].linkDetail,
									donvi: result[i].schoolName,
									loaihoatdong: result[i].tag,
									xacnhan: textLink.toString(),
									huy: textLinkRemove.toString()
								}, function(err, html){
									console.log("Bước 2");
									var subject = "Đề nghị xác nhận lại hoạt động";
									//Gửi Email thông qua API sendMail
									sendEmail.Send(req.user.email, subject, html);
								});
								tabvalues = 'manager-activities';
								res.redirect('/thong-tin');
							}
						}
					})
				})
			}
			// Nếu quyền là 3 thì liệt kê cả những hoạt động cấp khoa và trường
			else if (req.user.role == 3) {
				var query = {schoolOwner: req.user.uid};
				mongoose.model('account_list').find(query).exec(function(err, result1) {
					var temp = new Array();
					temp.push({school_id: req.user.uid})
					for (var i = 0; i < result.length; i++) {
						temp.push({school_id: result[i].uid});
					};
					var query = { $or: temp};
					var sort = {date: -1};
					mongoose.model('announcement').find(query).sort(sort).exec(function(err, result) {
						if (err) throw err;
						for (var i = 0; i < result.length; i++) {
							if (req.body.id == result[i]._id) {
								var textLink = "https://" + SERVER_URL + '/verify/announcement/' + req.user.id +'/' + result[i]._id +'/' + result[i].oneTimeCode +'/accept';
								var textLinkRemove = "https://" + SERVER_URL + '/verify/announcement/' + req.user.id +'/' + result[i]._id +'/' + result[i].oneTimeCode + '/remove';
								var tempa = getTemplate.getTemplateVerifyActivity();
								ejs.renderFile(tempa, {
									tenhoatdong: result[i].activity_name,
									thoigian: dateTime,
									diadiem: result[i].position,
									mota: result[i].introduction,
									motalink: result[i].linkDetail,
									donvi: result[i].schoolName,
									loaihoatdong: result[i].tag,
									xacnhan: textLink.toString(),
									huy: textLinkRemove.toString()
								}, function(err, html){
									console.log("Bước 2");
									var subject = "Đề nghị xác nhận lại hoạt động";
									//Gửi Email thông qua API sendMail
									sendEmail.Send(req.user.email, subject, html);

								});
								tabvalues = 'manager-activities';
								res.redirect('/thong-tin');
							}
						}
					})
				})
			}
			else if (req.user.role == 4) {
				var query1 = {school_id: req.user.uid};
				var sort = {date: -1};
				mongoose.model('announcement').find(query1).sort(sort).exec(function(err, result) {
					for (var i = 0; i < result.length; i++) {
						if (req.body.id == result[i]._id) {
							var textLink = "https://" + SERVER_URL + '/verify/announcement/' + req.user.id +'/' + result[i]._id +'/' + result[i].oneTimeCode +'/accept';
							var textLinkRemove = "https://" + SERVER_URL + '/verify/announcement/' + req.user.id +'/' + result[i]._id +'/' + result[i].oneTimeCode + '/remove';
							var tempa = getTemplate.getTemplateVerifyActivity();
							ejs.renderFile(tempa, {
								tenhoatdong: result[i].activity_name,
								thoigian: dateTime,
								diadiem: result[i].position,
								mota: result[i].introduction,
								motalink: result[i].linkDetail,
								donvi: result[i].schoolName,
								loaihoatdong: result[i].tag,
								xacnhan: textLink.toString(),
								huy: textLinkRemove.toString()
							}, function(err, html){
								console.log("Bước 2");
								var subject = "Đề nghị xác nhận lại hoạt động";
								//Gửi Email thông qua API sendMail
								sendEmail.Send(req.user.email, subject, html);

							});
							tabvalues = 'manager-activities';
							res.redirect('/thong-tin');
						}
					}
				})
			}

		}
		else if (req.body.loailenh == 'deleteActivity') {
			// Nếu quyền là 1, 2 thì liệt kê tất cả hoạt động
			if (req.user.role <= 2) {
				var query = {schoolOwner: req.user.uid};
				mongoose.model('account_list').find(query).exec(function(err, result1) {
					var sort = {date: -1};
					mongoose.model('announcement').find().sort(sort).exec(function(err, result) {
						if (err) throw err;
						for (var i = 0; i < result.length; i++) {
							if (req.body.id == result[i]._id) {
								var query = {school_id: result[i].school_id, activity_name: result[i].activity_name, date: result[i].date};
								mongoose.model('announcement').remove(query).exec(function(err, result) {
									if (err) throw err;
									tabvalues = 'manager-activities';
									res.redirect('/thong-tin');
								})
							}
						}
					})
				})
			}
			// Nếu quyền là 3 thì chỉ liệt kê các hoạt động của khoa và trường
			if (req.user.role == 3) {
				var query = {schoolOwner: req.user.uid};
				mongoose.model('account_list').find(query).exec(function(err, result1) {
					var temp = new Array();
					temp.push({school_id: req.user.uid})
					for (var i = 0; i < result.length; i++) {
						temp.push({school_id: result[i].uid});
					};
					var query = { $or: temp};
					var sort = {date: -1};
					mongoose.model('announcement').find(query).sort(sort).exec(function(err, result) {
						if (err) throw err;
						for (var i = 0; i < result.length; i++) {
							if (req.body.id == result[i]._id) {
								var query = {school_id: result[i].school_id, activity_name: result[i].activity_name, date: result[i].date};
								mongoose.model('announcement').remove(query).exec(function(err, result) {
									if (err) throw err;
									tabvalues = 'manager-activities';
									res.redirect('/thong-tin');
								})
							}
						}
					})
				})
			}
			else if (req.user.role == 4) {
				var query = {school_id: req.user.uid};
				var sort = {date: -1};
				mongoose.model('announcement').find(query).sort(sort).exec(function(err, result) {
					if (err) throw err;
					for (var i = 0; i < result.length; i++) {
						if (req.body.id == result[i]._id) {
							var query = {school_id: result[i].school_id, activity_name: result[i].activity_name, date: result[i].date};
							mongoose.model('announcement').remove(query).exec(function(err, result) {
								if (err) throw err;
								tabvalues = 'manager-activities';
								res.redirect('/thong-tin');
							})
						}
					}
				})
			}

		}

	} else {
		res.redirect('/login');
	};
});

module.exports = router;
