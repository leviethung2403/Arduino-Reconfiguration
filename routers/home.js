

var express = require('express'),
	router = express.Router(),
    multer  = require('multer'),
    path = require('path'),
    arduino = require('../service/arduino/arduino.service'),
    async = require('async'),
    mkdirp = require('mkdirp'),
    randomstring = require("randomstring")

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        let randString = randomstring.generate(5)
        mkdirp(path.join(uploads, randString), (err, res)=>{
            return cb(err, res)
        })
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '-' + file.originalname )
    }
})

let upload = multer({ storage: storage })

router.get('/', (req, res)=>{
    if (req.isAuthenticated()){
        res.render('home', {
            title: 'Đăng tải kịch bản thực thi',
            logged: null,
            page: 'home',
            result: [''],
            boxTitle: 'Upload sketch'
        })
    } else {
        res.redirect('/login')
    }
})
router.post('/', upload.any(), (req, res)=>{
    const body = req.body
    const originalname = req.files[0].originalname
    if (req.isAuthenticated() && body) {
        let respone = {}
        if (req.files[0] && req.files[0].filename) {
            respone = {
                status: 200,
                success: true
            }
        } else {
            respone = {
                status: 400,
                error: true
            }
        }
        io.emit('uploadFile', 'File uploaded, \nhost is verifing sketch to store.')
        req.files.map(file => {
            async.waterfall([
                cb => arduino.verify(file.path, file.destination, cb),
                (build_folder, sketch_path, cb) => {
                    arduino.rmdir(build_folder, sketch_path, cb)
                },
                (filename, cb) => arduino.store(req.user.username, filename, body, originalname, cb)
            ], (_err, _result) => {
                if (_err) {
                    console.error(_err)
                    return res.end(JSON.stringify('User doesn\'t have permission.'))
                } else res.end(JSON.stringify(respone))
            })
        })
    } else {
        return res.end(JSON.stringify('User doesn\'t have permission.'))
    }
})


module.exports = router;
