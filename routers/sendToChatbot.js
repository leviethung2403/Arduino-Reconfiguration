

var express = require('express'),
	router = express.Router(),
	funcSendNotifyOrMessageToChatbot = require('./../api/sendNotifyOrMessageToChatbot');
	

router.get('/', (req, res) => {
	res.redirect('/chatbot/them-hoat-dong');
})

router.get('/them-hoat-dong', (req, res) => {
	if(req.isAuthenticated()) {
		res.render('addActivity', {profile_pic: req.user.profile_pic, page: 'addActivity', logged : true, name: req.user.uid, donvi: req.user.username, role: req.user.role});
	} else {
		res.redirect('/login');
	};
});

router.post('/them-hoat-dong', (req, res) => {
	if(req.isAuthenticated()) {
		funcSendNotifyOrMessageToChatbot.SaveActivity(req.user.uid, req.body, req);
		res.redirect('/chatbot/them-hoat-dong');
	} else {
		res.redirect('/login');
	};
});

router.get('/gui-tin-nhan', (req, res) => {
	if(req.isAuthenticated()) {
		res.render('sendMessageToFollowers', {profile_pic: req.user.profile_pic, page: 'addActivity', logged : true, name: req.user.uid, donvi: req.user.username, role: req.user.role});
	} else {
		res.redirect('/login');
	};
});

router.post('/gui-tin-nhan', (req, res) => {
	if(req.isAuthenticated()) {
		funcSendNotifyOrMessageToChatbot.SaveMessage(req.user.uid, req.body, req);
		res.redirect('/chatbot/them-hoat-dong');
	} else {
		res.redirect('/login');
	};
});


module.exports = router;