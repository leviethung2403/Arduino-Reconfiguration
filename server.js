
'use strict'

const
	mongodb = require('./lib/database/mongodb'),
    Passport = require('passport'),
	express = require('express'),
	bodyParser = require('body-parser'),
	path = require('path'),
    cookieParser = require('cookie-parser'),
    session = require('express-session')

var app = express()
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser())

app.set('view engine', 'ejs')
app.set('views', './views')
app.set('trust proxy', 1)

global.config = require('./config')
global.uploads = path.join(__dirname, 'uploads')

app.use(session({
    name: 'reconfig_cookie',
    proxy: true,
    resave: true,
    saveUninitialized: true,
    secret: 'reconfig_cookie_secret',
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 // 24 tiếng
    }
}))

let http = require('http').Server(app)
global.io = require('socket.io')(http)
io.on('connection', socket => {
    console.log('New connection comming from: ', socket.id)
})

app.use(Passport.initialize())
app.use(Passport.session())
mongodb.openConnection(config.default_connect_string)

require('./service/authenticate/index')(app)
require('./routers/index')(app)

let port = process.env.PORT || 3004
http.listen(port)
console.log('App is listening at port ' + port)