const nodemailer = require('nodemailer'),
  config = require('config'),
  express = require('express'),
  mongoose = require('mongoose'),
  ejs = require('ejs'),
  bcrypt = require('bcryptjs'),
  randomstring = require('randomstring'),
  sendEmail = require('./sendEmail')

module.exports = {
  Check: function (userid, current_pass, new_password, reType_password, password_in_database) {
    bcrypt.compare(current_pass, password_in_database, function (err, isMatch) {
      if (err) throw err
      if (isMatch) {
        if ((new_password === '') || (reType_password === ''))
          console.log('Mật khẩu nhập lại trống')
        // res.redirect(direct); //2 Mật khẩu nhập lại trống
        else if ( (new_password != reType_password))
          console.log('Mật khẩu mới không trùng nhau')
        // res.redirect(direct); //Mật khẩu mới không trùng nhau.
        else if ( (new_password === current_pass)) {
          console.log('Mật khẩu mới không giống mật khẩu cũ')
        // res.redirect(direct); //Mật khẩu mới giống mật khẩu cũ.
        }
        else if( (new_password === reType_password)) { // Hash mật khẩu vào update database.
          console.log('Đã vào bước này 1')

          bcrypt.hash(new_password, 10, function (err, hash) {
            var query = {uid: userid}
            var update = {password: hash}
            var option = {new: false}
            console.log('Đã vào bước này')

            mongoose.model('account_list').findOneAndUpdate(query, update, option, function (err, userDoc) {
              if (err) {
                console.log('Error when save data.')
              } else {
                var randomCode = randomstring.generate(3)
                ejs.renderFile(__dirname + '/email_template/passwordWasChange.ejs', {
                  taikhoan: userDoc.uid,
                  matkhau: new_password,
                  tendonvi: userDoc.username,
                  tructhuoc: userDoc.schoolOwner,
                  randomCode: randomCode
                }, function (err, html) {
                  if (err) throw err
                  var subject = 'Mật khẩu đã được thay đổi bởi ' + userDoc.schoolOwner
                  // Gửi Email thông qua API sendMail
                  sendEmail.Send(userDoc.email, subject, html)
                })
              }
            })
          })
        }
      }else {
        console.log('Mật khẩu không khớp')
      // res.redirect(direct); //Mật khẩu hiện tại không khớp.
      }
    })
  },
  CheckWithoutCompare: function (userid, new_password, reType_password) {
    if ((new_password === '') || (reType_password === ''))
      console.log('Mật khẩu nhập lại trống')
    // res.redirect(direct); //2 Mật khẩu nhập lại trống
    else if ( (new_password != reType_password))
      console.log('Mật khẩu mới không trùng nhau')
    // res.redirect(direct); //Mật khẩu mới không trùng nhau.
    else if( (new_password === reType_password)) { // Hash mật khẩu vào update database.
      console.log('Đã vào bước này 1')

      bcrypt.hash(new_password, 10, function (err, hash) {
        var query = {uid: userid}
        var update = {password: hash}
        var option = {new: false}
        console.log('Đã vào bước này')
        mongoose.model('account_list').findOneAndUpdate(query, update, option, function (err, userDoc) {
          if (err) {
            console.log('Error when save data.')
          } else {
            var randomCode = randomstring.generate(3)
            ejs.renderFile(__dirname + '/email_template/passwordWasChange.ejs', {
              taikhoan: userDoc.uid,
              matkhau: new_password,
              tendonvi: userDoc.username,
              tructhuoc: userDoc.schoolOwner,
              randomCode: randomCode
            }, function (err, html) {
              if (err) throw err
              var subject = 'Mật khẩu đã được thay đổi bởi ' + userDoc.schoolOwner
              // Gửi Email thông qua API sendMail
              sendEmail.Send(userDoc.email, subject, html)
            })
          }
        })
      })
    }
  }
}
