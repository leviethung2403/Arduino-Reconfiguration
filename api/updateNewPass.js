

const
	mongoose = require('mongoose')

module.exports = {
	updateNewPassword: function(schoolId, newPass) {
		var query = {school_id: schoolId};
		var update = {password: newPass };
		var option = {new : false};
		mongoose.model('school').findOneAndUpdate(query, update, option, function (err, userDoc){
			if (err)
				console.log('Error when save data.');
			else {
				console.log("Mật khẩu mới đã được lưu");
			}
		});
	}
};