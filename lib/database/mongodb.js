

const mongoose = require('mongoose')

class MongoDB{
    static openConnection(connect_string = null){
        mongoose.Promise = global.Promise
        let mongoose_options = {
            useMongoClient: true
        }
        mongoose.connect(connect_string || config.default_connect_string, mongoose_options)
        let db = mongoose.connection
        db.on('error', console.error.bind(console, 'connection error'))
        db.once('open', () => {
            console.log('Database is open')
        })
    }
}

module.exports = MongoDB
